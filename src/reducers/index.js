import { combineReducers } from "redux";

export const FETCHED = "fetched";

export function fetchedComments(item) {
  console.log(item);
  return { type: FETCHED, payload: item };
}

const INITIAL_STATE = {
  list: [],
};
const Reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCHED:
      console.log(state);
      return {
        ...state,
        list: [...action.payload],
      };
    default:
      return state;
  }
};

export const allReducer = combineReducers({
  comments: Reducer,
});

export default allReducer;

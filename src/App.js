import React, { useState } from "react";
import logo from "./logo.svg";
import "./App.css";
import api from "./api.js";
import { useDispatch, useSelector } from "react-redux";
import { fetchedComments } from "./reducers";

function List(props) {
  const numbers = props.numbers;
  const listItems = numbers.map((number) => <li>{number["postId"]}</li>);
  return <ul>{listItems}</ul>;
}

function App() {
  const redux = useSelector((state) => state);
  const dispatch = useDispatch();
  const setComments = (list_of_comments) =>
    dispatch(fetchedComments(list_of_comments));
  const [list, setList] = useState(redux.comments.list);

  const fetchData = (e) => {
    e.preventDefault();

    api
      .getData()
      .then((response) => {
        console.log(response.data);
        setComments(response.data);
        //setList(response.data)
      })
      .catch((error) => {
        console.log(error);
      });
  };
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <button onClick={(e) => fetchData(e)} type="button">
          Pull Data
        </button>
        <List numbers={list} />
      </header>
    </div>
  );
}

export default App;
